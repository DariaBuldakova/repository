#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

const int SIZE_MATRIX = 15, MIN_RAND = -6, MAX_RAND = 6;

void MaxElement(int matrix[SIZE_MATRIX][SIZE_MATRIX]); //���������� �������� � �������� ������� � ������� ��������
void AscendingElementStr(int matrix[SIZE_MATRIX][SIZE_MATRIX]); //������, � ������� ��������, ������������� ����� ����������� � ������������ ���������� � ������, �������� ������������ ������������������
void MinElement(int matrix[SIZE_MATRIX][SIZE_MATRIX]); //����������� ������� ������� � ���������� �������
void PrintMatrix(int matrix[SIZE_MATRIX][SIZE_MATRIX]);
void CreateMatrix(int matrix[SIZE_MATRIX][SIZE_MATRIX]); //�������� �������
bool Question(bool& check);//����������� � ����
void KolMaxElement(int matrix[SIZE_MATRIX][SIZE_MATRIX]); //��������� ���������� ������������ ���������
void RearrangeBlocks(int matrix[SIZE_MATRIX][SIZE_MATRIX]); //���������� ������������ ������ 2 � 8


void main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "");

	int i, j, number;
	bool check = 1;

	int matrix[SIZE_MATRIX][SIZE_MATRIX]{};
	CreateMatrix(matrix);

	cout << "������� �������:" << endl;
	PrintMatrix(matrix);

	while (check)
	{
		cout << "1 - ����� ���������� �������� � �������� ������� � ������� ��������" << endl;
		cout << "2 - ����� ������, � ������� ��������, ������������� ����� ����������� � ������������ ���������� � ������, �������� ������������ ������������������" << endl;
		cout << "3 - ����� ����������� ������� ������� � ���������� �������" << endl;
		cout << "4 - ��������� ���������� ������������ ���������" << endl;
		cout << "5 - ���������� ������������ ������ 2 � 8" << endl;
		cout << "0 - ��������� ���������" << endl;

		cin >> number;

		system("cls");

		switch (number)
		{
		case 0:
			check = 0;
			break;
		case 1:
			MaxElement(matrix);
			Question(check);
			break;
		case 2:
			AscendingElementStr(matrix);
			Question(check);
			break;
		case 3:
			MinElement(matrix);
			Question(check);
			break;
		case 4:
			KolMaxElement(matrix);
			Question(check);
			break;
		case 5:
			RearrangeBlocks(matrix);
			Question(check);
			break;
		default:
			check = 0;
			break;
		}

		system("cls");
	}

	system("pause");
}


void MaxElement(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	PrintMatrix(matrix);

	cout << "\n���������� �������� � �������� ������� � ������� ��������\n";

	int max = -7;

	for (int j = 0; j < SIZE_MATRIX; j += 2) {
		for (int i = 0; i < SIZE_MATRIX; i++) {
			if (matrix[i][j] > max)
			{
				max = matrix[i][j];
			}
		}
		cout << "���������� ������� � ������� " << j << " = " << max << endl;
		max = 0;
	}
	cout << endl;
}


void MinElement(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	PrintMatrix(matrix);

	cout << "\n����������� ������� ������� � ���������� �������\n";

	int InitialValue = SIZE_MATRIX, EndValue = 0, min = 7;

	for (int i = SIZE_MATRIX - 1; i >= (SIZE_MATRIX / 2); i--) {
		for (int j = EndValue; j < InitialValue; j++) {
			if (matrix[i][j] < min)
			{
				min = matrix[i][j];
			}
		}
		EndValue++;
		InitialValue--;
	}

	cout << "����������� ������� = " << min << endl;
}


void AscendingElementStr(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	PrintMatrix(matrix);
	cout << "\n������, � ������� ��������, ������������� ����� ����������� � ������������ ���������� � ������, �������� ������������ ������������������\n";

	int min = 7, PositionMin, PositionMax, max = -7, i, j, k;

	for (i = 0; i < SIZE_MATRIX; i++) {
		for (j = 0; j < SIZE_MATRIX; j++) {
			if (matrix[i][j] > max)
			{
				PositionMax = j;
				max = matrix[i][j];
			}
			if (matrix[i][j] < min)
			{
				PositionMin = j;
				min = matrix[i][j];
			}
		}

		if (abs(PositionMin - PositionMax) - 1 < 2)
		{
			cout << "������ �" << i << " �� ��������" << endl;
		}
		else if (PositionMin < PositionMax)
		{
			k = 1;
			for (j = PositionMin + 2; j < PositionMax; j++) {

				if (matrix[i][j] > matrix[i][j - 1])
				{
					k++;
				}
			}
			if (k == PositionMax - PositionMin - 1)
			{
				cout << "������ �" << i << endl;
				for (k = 0; k < SIZE_MATRIX; k++) {
					cout << setw(5) << matrix[i][k];
				}
				cout << endl;
			}
			else
			{
				cout << "������ �" << i << " �� ��������" << endl;
			}
			min = 7;
			max = -7;
		}
		else if (PositionMin > PositionMax)
		{
			k = 1;
			for (j = PositionMax + 2; j < PositionMin; j++) {

				if (matrix[i][j] > matrix[i][j - 1])
				{
					k++;
				}
			}
			if (k == PositionMin - PositionMax - 1)
			{
				cout << "������ �" << i << endl;
				for (k = 0; k < SIZE_MATRIX; k++) {
					cout << setw(5) << matrix[i][k];
				}
				cout << endl;
			}
			else
			{
				cout << "������ �" << i << " �� ��������" << endl;
			}
		}
		min = 7;
		max = -7;
	}
	cout << endl;
}

void PrintMatrix(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	for (int i = 0; i < SIZE_MATRIX; i++) {
		for (int j = 0; j < SIZE_MATRIX; j++) {
			cout << setw(5) << matrix[i][j];
		}
		cout << endl;
	}
}

void CreateMatrix(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	for (int i = 0; i < SIZE_MATRIX; i++) {
		for (int j = 0; j < SIZE_MATRIX; j++) {
			matrix[i][j] = rand() % (MAX_RAND - (MIN_RAND)+1) + MIN_RAND;
		}
	}
}

bool Question(bool& check)
{
	cout << "\n������ ��������� � ����? �� - 1, ��� - 0\n";
	cin >> check;
	if (check != 1)
	{
		check = 0;
	}

	return check;
}

void KolMaxElement(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	PrintMatrix(matrix);

	int kol = 0;
	
	for (int i = 0; i < SIZE_MATRIX; i++) {
		for (int j = 0; j < SIZE_MATRIX; j++) {
			if (matrix[i][j] == MAX_RAND)
			{
				kol++;
			}
		}
	}

	cout << "���������� ������������ ��������� = " << kol << endl;
}


void RearrangeBlocks(int matrix[SIZE_MATRIX][SIZE_MATRIX])
{
	PrintMatrix(matrix);

	int i, j;

	int _matrix[SIZE_MATRIX][SIZE_MATRIX]{};

	for (int i = 0; i < SIZE_MATRIX; i++) {
		for (int j = 0; j < SIZE_MATRIX; j++) {
			_matrix[i][j] = matrix[i][j];
		}
	}

	for (i = 0; i < 5; i++) {
		for (j = 5; j < 10; j++) {
			_matrix[i + 10][j] = matrix[i][j];
		}
	}

	for (i = 10; i < 15; i++) {
		for (j = 5; j < 10; j++) {
			_matrix[i - 10][j] = matrix[i][j];
		}
	}

	for (int i = 0; i < SIZE_MATRIX; i++) {
		for (int j = 0; j < SIZE_MATRIX; j++) {
			matrix[i][j] = _matrix[i][j];
		}
	}

	cout << "\n����� ������������\n" << endl;
	PrintMatrix(matrix);
}